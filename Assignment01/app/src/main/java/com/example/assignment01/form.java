package com.example.assignment01;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class form extends AppCompatActivity {
    Button button;
    EditText usrname,pswrd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
    button=findViewById(R.id.btn);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(form.this);
        builder1.setMessage("Email and Password are not equal");
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        usrname=findViewById(R.id.nameEdit);
        pswrd =findViewById(R.id.password);
        button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String email = usrname.getText().toString().trim();
            String password= pswrd.getText().toString().trim();
            if(email.equals(password))
            {
                Toast.makeText(form.this,"Email and Password are equal",Toast.LENGTH_LONG).show();
            }
            else {
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
    });




    }




}