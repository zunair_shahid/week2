package com.example.assignment_04_weather_app;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class show_weather_info extends AppCompatActivity {
    TextView textView1, textView2, textView3, textView4, textView5, textView6, textView7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_weather_info);
        textView1 =findViewById(R.id.tv1);
        textView2 =findViewById(R.id.tv2);
        textView3 =findViewById(R.id.tv3);
        textView4 =findViewById(R.id.tv4);
        textView5 =findViewById(R.id.tv5);
        textView6 =findViewById(R.id.tv6);
        textView7 =findViewById(R.id.tv7);
        Intent intent = getIntent();
        String city = intent.getStringExtra("show_weather_info");
        String api_key="f0f2ca761f89c106b7869ee3f9d1a88d";
        String url="http://api.weatherstack.com/current?access_key=f0f2ca761f89c106b7869ee3f9d1a88d&query="+city;
        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject object=response.getJSONObject("request");
                    JSONObject current=response.getJSONObject("current");
                    String que1=object.getString("query");
                    String desc=current.getString("weather_descriptions");
                    String temperature=current.getString("temperature");
                    String wind_speed=current.getString("wind_speed");
                    String wind_dir=current.getString("wind_dir");
                    String precip=current.getString("precip");
                    String humidity=current.getString("humidity");
                    textView1.setText("Temperature: "+temperature+" C");
                    textView2.setText("Humidity: "+humidity);
                    textView3.setText("Wind Speed: "+wind_speed);
                    textView4.setText("Description: "+desc.replaceAll("[\\[\\](){}]",""));
                    textView5.setText("Wind direction: "+wind_dir);
                    textView6.setText("Precipitation: "+precip);
                    textView7.setText("City , Country: "+que1);
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(show_weather_info.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(request);
    }
}